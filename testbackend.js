
async function test() {
    try {
        const res = await fetch("http://127.0.0.1:5000/api/donations");
        if (!res.ok) {
            throw new Error(`HTTP error! status: ${res.status}`);
        }
        const data = await res.json();
        console.log(data);
    } catch (e) {
        console.error(e);
    }
}

test();
console.log("ran");
