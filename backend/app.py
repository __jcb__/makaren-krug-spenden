from flask import Flask, request, jsonify, render_template, redirect, url_for
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources={r"/api/*": {"origins": "*"}})

# Sample data
donations = [
    {"id": 0, "name": "Average Joe", "amount": 1,  "Fuxenstall": 2010},
    {"id": 1, "name": "John Doe", "amount": 10, "Fuxenstall": 2009},
    {"id": 2, "name": "Jane Smith", "amount": 20, "Fuxenstall": 2009},
    {"id": 3, "name": "Alice Johnson", "vulgo": "ignatz", "amount": 25, "Fuxenstall": 2008},
    {"id": 4, "name": "Lars Chrismas",  "amount": 50, "Fuxenstall": -1},
     {"id": 5, "name": "Letts McOut", "amount": 25, "Fuxenstall": -1},
    {"id": 6, "name": "Ben Dover",  "amount": 50, "Fuxenstall": 2006},
]

@app.route('/api/donations', methods=['GET'])
def get_donations():
    print("hit backend")
    return jsonify(donations)

@app.route('/admin', methods=['GET', 'POST'])
def admin():
    if request.method == 'POST':
        name = request.form.get('name')
        amount = request.form.get('amount')
        if name and amount:
            new_id = len(donations) + 1
            donations.append({"id": new_id, "name": name, "amount": int(amount)})
        return redirect(url_for('admin'))
    return render_template('admin.html', donations=donations)

if __name__ == '__main__':
    app.run(debug=True)
